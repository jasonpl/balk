import urwid


from .types import DisplayText, Game, GameState


class Menu(urwid.WidgetWrap):
    def __init__(self, display_text: DisplayText):
        self.display_text = display_text

        # (Example maximum: " CLE  2 - 3  CIN  F/10 ")
        self.menu_width = 23
        self.overview_pane = self.create_overview_pane()
        self.detail_pane = self.create_detail_pane()
        self.columns = self.create_columns(
            self.overview_pane, self.detail_pane)

        self.palette = [(None, "white", "black"),
                        ("options", "white", "black"),
                        ("focus heading", "white", "dark red"),
                        ("focus line", "black", "dark red"),
                        ("focus options", "black", "light gray"),
                        ("selected", "white", "dark blue"),
                        ("hidden", "black", "black")]

        # it's actually totally fine to run right off the bottom
        lines = 500
        self.loop = urwid.MainLoop(urwid.Filler(self.columns, "top", lines),
                                   unhandled_input=self.handle_input,
                                   palette=self.palette,
                                   event_loop=urwid.AsyncioEventLoop())
        self.loop.set_alarm_in(0, self._update)
        try:
            self.loop.run()
        except KeyboardInterrupt:
            self.loop.stop()

    def _gameline(self, game: Game):
        match game.game_state:
            case GameState.PREGAME | GameState.DELAY:
                start_str = game.start_time.strftime("%H:%M")
                score = f"{game.away_team:<3}  {start_str:>5}  {game.home_team:<3}"
            case GameState.POSTPONED:
                score = f"{game.away_team:<3}   ━━━   {game.home_team:<3}"
            case _:
                away_team_str = f"{game.away_team:<3} {game.away_team_total_runs:>2}"
                home_team_str = f"{game.home_team_total_runs:<2} {game.home_team:<3}"
                score = f"{away_team_str} ─ {home_team_str}"

        # state can be up to four characters (longest case is F/##)
        if game.game_state == GameState.LIVE:
            arrow = "▲" if game.is_top_of_inning else "▼"
            state = f"{arrow}{game.current_inning:<2}"
        elif game.game_state == GameState.FINAL:
            if game.current_inning and game.current_inning > 9:
                state = f"F/{game.current_inning:2}"
            else:
                state = "F"
        elif game.game_state == GameState.POSTPONED:
            state = "post"
        elif game.game_state == GameState.PREGAME:
            state = ""  # the start time implies PREGAME
        elif game.game_state == GameState.DELAY:
            state = "dlay"
        else:
            logging.error(f"tried to make a gameline with unknown state {game_state}")
            state = "????"
        game_str = f"{score}  {state:<4}"

        # -2 for a blank on both sides
        return game_str[0:self.menu_width - 2]

    def _update(self, loop=None, data=None):
        self.update_overview_pane()
        self.update_detail_pane()
        self.loop.set_alarm_in(1, self._update)

    def create_columns(self, overview_menu, detail_window):
        columns = urwid.Columns([], dividechars=1)
        columns.contents.append(
            (urwid.AttrMap(overview_menu,
                           "focus options"), columns.options("given", self.menu_width)))
        columns.contents.append(
            (urwid.AttrMap(detail_window,
                           "options"), columns.options("weight")))

        return columns

    def item_chosen(self, button, choice):
        if choice == "exit":
            self.exit_program()

        self.display_text.select_game(choice)

    def create_overview_pane(self):
        buttons = []
        for game in self.display_text.games:
            button = MenuButton(self._gameline(game), self.item_chosen, game)
            buttons.append(button)

        title = urwid.AttrMap(urwid.Text(
            [u"\n", self.display_text.title], align="center"), "focus heading")
        divider = urwid.AttrMap(urwid.Divider(
            u"\N{LOWER ONE QUARTER BLOCK}"), "focus line")
        self.game_button_list = urwid.ListBox(
            urwid.SimpleFocusListWalker(buttons))
        game_buttons = urwid.AttrMap(self.game_button_list, "focus options")

        return urwid.Frame(game_buttons, urwid.Pile([title, divider, urwid.Divider()]))

    def create_detail_pane(self):
        self.overlay_widget = urwid.Text("\n\n\n\n")
        self.box_score_widget = urwid.Text("")
        self.weather_widget = urwid.Text("")
        self.ab_widget = urwid.Text("")
        self.play_by_play_widget = urwid.Text("")

        self.divider = urwid.AttrMap(urwid.Divider(u"━"), None)

        sub_columns = urwid.Columns([], dividechars=1)
        sub_columns.contents.append(
            (self.overlay_widget, sub_columns.options("given", 25)))
        sub_columns.contents.append(
            (self.box_score_widget, sub_columns.options("given", 42)))
        sub_columns.contents.append(
            (self.weather_widget, sub_columns.options("given", 15)))

        detail_columns = urwid.Columns([(23, self.ab_widget),
            self.play_by_play_widget], dividechars=3)
        return urwid.Filler(urwid.Pile([sub_columns, self.divider,
            detail_columns]), valign="top")

    def update_overview_pane(self):
        # TODO: handle update across time boundary (more/fewer games - maybe delete/refresh buttons?)

        for index, button in enumerate(self.game_button_list.body):
            game = self.display_text.games[index]
            label = self._gameline(game)
            button.set_label(label)
            button._w = urwid.AttrMap(urwid.SelectableIcon(
                [f" {label}"], cursor_position=self.menu_width), None, focus_map="selected")

    def update_detail_pane(self):
        if self.display_text.open_game is None:
            return

        self.overlay_widget.set_text(self.display_text.overlay_str)
        self.box_score_widget.set_text(self.display_text.box_score_str)
        self.weather_widget.set_text(self.display_text.weather_str)
        self.ab_widget.set_text(self.display_text.ab_str)
        self.play_by_play_widget.set_text(self.display_text.play_by_play_str)

    def handle_input(self, key):
        if key == "q" or key == "Q":
            self.exit_program()
        if key == "down":
            self.game_button_list.focus_position = 0
        elif key == "up":
            self.game_button_list.focus_position = \
                len(self.game_button_list.body) - 1

    def exit_program(self):
        raise urwid.ExitMainLoop()


class MenuButton(urwid.Button):
    def __init__(self, label, callback, user_data):
        super(MenuButton, self).__init__(
            str(user_data), on_press=callback, user_data=user_data)
        self._w = urwid.AttrMap(urwid.SelectableIcon(
            [f" {label}"], cursor_position=len(label) + 2), None, focus_map="selected")
