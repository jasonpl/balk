import datetime
import enum
import logging
import threading
import time
from dataclasses import dataclass, field
from typing import List, Optional


class Team(enum.Enum):
    # AL East
    BAL = enum.auto()
    BOS = enum.auto()
    NYY = enum.auto()
    TB = enum.auto()
    TOR = enum.auto()

    # AL Central
    CLE = enum.auto()
    CWS = enum.auto()
    DET = enum.auto()
    KC = enum.auto()
    MIN = enum.auto()

    # AL West
    HOU = enum.auto()
    LAA = enum.auto()
    OAK = enum.auto()
    SEA = enum.auto()
    TEX = enum.auto()

    # NL East
    ATL = enum.auto()
    MIA = enum.auto()
    NYM = enum.auto()
    PHI = enum.auto()
    WSH = enum.auto()

    # NL Central
    CHC = enum.auto()
    CIN = enum.auto()
    MIL = enum.auto()
    PIT = enum.auto()
    STL = enum.auto()

    # NL West
    ARI = enum.auto()
    AZ = enum.auto() # Changed for 2023? Leaving both versions for now
    COL = enum.auto()
    LAD = enum.auto()
    SD = enum.auto()
    SF = enum.auto()

    # All-Stars
    AL = enum.auto()
    NL = enum.auto()

    def __str__(self):
        return self.name


class GameState(enum.Enum):
    PREGAME = enum.auto()
    LIVE = enum.auto()
    FINAL = enum.auto()
    DELAY = enum.auto()
    # MLB doesn't have a code for this. It's represented in multiple fields.
    POSTPONED = enum.auto()
    UNKNOWN = enum.auto()

    @staticmethod
    def from_code(code):
        if code == "P":
            return GameState.PREGAME
        if code == "D":
            return GameState.DELAY
        if code == "F":
            return GameState.FINAL
        if code == "L":
            return GameState.LIVE
        return GameState.UNKNOWN


@dataclass
class FieldState:
    """ Represents the most rapidly-changing info about who is standing where
    and the usual baseball counts. """

    pitcher: str = ""
    batter: str = ""
    on_deck: str = ""
    in_hole: str = ""

    num_outs: int = 0
    num_balls: int = 0
    num_strikes: int = 0

    is_runner_on_first: bool = False
    is_runner_on_second: bool = False
    is_runner_on_third: bool = False


class Game:
    """ Each instance uniquely represents one game. Should be created from the
    schedule api, then updated from the details API. """

    def __init__(self, start_time: datetime.datetime, home_team: Team,
            away_team: Team, game_id: int, stadium: str, city: str, details_url: str) -> None:
        # headline info
        self.start_time = start_time
        self.home_team = home_team
        self.away_team = away_team
        self.stadium = stadium
        self.city = city
        # If True, indicates an issue retrieving weather for the stadium and
        # that weather should be retrieved for the city instead.
        self.use_city_weather = False

        # game state
        self.current_inning = 0
        self.is_top_of_inning = False
        self.game_state = GameState.UNKNOWN
        self.field_state = FieldState()

        # box score
        # TODO: make box score a class?
        self.away_team_total_runs = 0
        self.away_team_total_hits = 0
        self.away_team_total_errors = 0
        self.away_team_box_score_innings = [None]
        self.home_team_total_runs = 0
        self.home_team_total_hits = 0
        self.home_team_total_errors = 0
        self.home_team_box_score_innings = [None]

        # resource for a particular game given by the schedule. Used to request game details from the API
        self.game_id = game_id
        self.details_url = details_url

        # time at which the game should next update details
        self.next_update_time: float = 0
        self.next_weather_update_time: Optional[float] = None

        # list of most recent plays for the current game
        self.plays: List[Play] = []

        # weather, if game is subject to it
        self.weather: Optional[Weather] = None

    def __str__(self):
        return f"Game({self.away_team}@{self.home_team}, {self.start_time})"

    def __lt__(self, other):
        return self.start_time < other.start_time


def _pitch_type(pitch_code: str) -> str:
    """
    Translate MLB's pitch codes to something easier to read.
    see https://www.mlb.com/glossary/pitch-types
    """
    code_map = {
            "CH": "CHNG",
            "CU": "CURV",
            "EP": "EEPH",
            "FA": "4SFB",
            "FC": "CUT",
            "FF": "4SFB",   # glossary says FA, but we're getting FF
            "FO": "FORK",
            "FS": "SPLT",
            "FT": "2SFB",
            "KC": "KNCV",
            "KN": "KNUC",
            "SC": "SCRW",
            "SI": "SINK",
            "SL": "SLID",
            "ST": "SWPR",
    }
    if pitch_code not in code_map:
        logging.warn(f"unknown pitch code {pitch_code}")
    return code_map.get(pitch_code, "????")

@dataclass
class Pitch:
    """
    Represents one pitch and its outcome.
    """
    detailed_description: str = ""
    pitch_code: str = ""
    pitch_speed: float = 0.0
    hit_speed: float = 0.0
    is_in_play: bool = False
    is_fouled: bool = False
    is_strike: bool = False
    is_ball: bool = False

    def short_str(self):
        pitch_type = _pitch_type(self.pitch_code)

        result = "???"
        if self.is_in_play:
            result = "in play"
        elif self.is_fouled:
            result = "foul"
        elif self.is_strike:
            result = "strike"
        elif self.is_ball:
            result = "ball"

        return f"{pitch_type:<4} {self.pitch_speed:>5} {result}"

@dataclass
class NoPitch:
    """
    Represents a call of no pitch.
    """
    event: str = "????"

    def short_str(self):
        match self.event:
            case "VP": return "clockvio pitcher"
            case "CA" | "VB":
                return "clockvio batter"
            case _: return self.event


@dataclass
class Balk:
    """
    The one true outcome.
    """
    def short_str(self):
        return "balk!"


@dataclass
class BatterTimeout:
    """
    Messing with the pitcher?
    """
    def short_str(self):
        return "batter timeout"


@dataclass
class CaughtStealing:
    """
    Did a crime. Didn't get away with it.
    """
    def __init__(self, base: int):
        self.base = base

    def short_str(self):
        return f"caught steal {base}B"


@dataclass
class DisengagementViolation:
    """
    You don't get unlimited pickoff attempts anymore.
    """
    def short_str(self):
        return "disengage vio"


@dataclass
class MoundVisit:
    """
    Stalling for time?
    """
    def short_str(self):
        return "mound visit"


@dataclass
class Pickoff:
    """
    TOOTBLAN
    """
    def __init__(self, base: int, success: bool):
        self.base = base
        self.success = success

    def short_str(self):
        base = self.base if self.base else "?"
        if self.success:
            return f"pickoff {base}B, out"
        else:
            return f"pickoff att {base}B"


@dataclass
class PassedBall:
    """
    When the catcher does an oopsie.
    """
    def short_str(self):
        return "passed ball"


@dataclass
class WildPitch:
    """
    When the pitcher does an oopsie.
    """
    def short_str(self):
        return "wild pitch"


@dataclass
class Play:
    """
    Represents one play, which is the outcome of a plate appearance or another
    event of similar importance.
    """

    description: str = ""
    # unused?
    detailed_description: str = ""
    event: str = ""
    eventType: str = ""
    time: str = ""
    sequence: List[Pitch] = field(default_factory=list)

    def __str__(self):
        return f"{self.description}\n\n"


def _f_to_c(c):
    return round((c * 9 / 5) + 32)


def _mps_to_mph(mps):
    return round(mps * 2.236936292)


@dataclass
class Weather:
    """ Represents current weather conditions relevant to baseball. """
    condition: str
    temp_c: float
    feelslike_c: float
    expected_precipitation: float
    wind_speed_metric: float
    wind_direction: str

    @property
    def temp_f(self):
        return _f_to_c(self.temp_c)

    @property
    def feelslike_f(self):
        return _f_to_c(self.feelslike_c)

    @property
    def wind_speed_imperial(self):
        return _mps_to_mph(self.wind_speed_metric)

    @property
    def condition_emoji(self):
        """ Documentation:
        https://api.met.no/weatherapi/weathericon/2.0/documentation
        """
        condition = self.condition
        night = condition.endswith("_night")
        condition = condition.split("_")[0]
        if condition in ("clearsky", "partlycloudy", "fair"):
            return ("☀️", "🌙")[night]
        if condition == "cloudy":
            return "☁️"
        if condition == "fog":
            return "🌫️"
        if "thunder" in condition:
            return "🌩️"
        if "rain" in condition:
            return "🌧️"
        if "snow" in condition or "sleet" in condition:
            return "❄️"
        logging.info(f"couldn't pick an emoji for weather={condition}")
        return condition[:3]


class Overlay:
    """
    Represents a string for the overlay, which includes the score, bases,
    inning, count, outs, pitch, hit, pitcher, and batter. Combination of
    elements from the game, field, and play objects.
    """

    def __init__(self, game: Game, field: FieldState):
        self.game = game
        self.field = field

    def __str__(self):
        final = self.game.game_state == GameState.FINAL
        if final:
            away_team_pip = " "
            home_team_pip = " "
            inning_str = "Final"
        else:
            if self.game.is_top_of_inning:
                away_team_pip = "●"
                home_team_pip = " "
            else:
                away_team_pip = " "
                home_team_pip = "●"

            inning_str = f"{'▲' if self.game.is_top_of_inning else '▼'} {self.game.current_inning}"

        away_team_str = f"{self.game.away_team.name:<5}{away_team_pip}"
        home_team_str = f"{self.game.home_team.name:<5}{home_team_pip}"

        away_team_score = self.game.away_team_total_runs
        home_team_score = self.game.home_team_total_runs

        first_base_icon = "◆" if self.field.is_runner_on_first else "◇"
        second_base_icon = "◆" if self.field.is_runner_on_second else "◇"
        third_base_icon = "◆" if self.field.is_runner_on_third else "◇"

        count_str = outs_str = pitcher_str = batter_str = ""
        if not final:
            count_str = f"{self.field.num_balls} ─ {self.field.num_strikes}"
            num_outs = self.field.num_outs
            for _ in range(3):
                if num_outs > 0:
                    outs_str += "● "
                else:
                    outs_str += "○ "

                num_outs -= 1
            outs_str = outs_str[:-1]

            pitcher_str = batter_str = ""
            if self.field.pitcher:
                pitcher_str = f"P:  {self.field.pitcher} "
            if self.field.batter:
                batter_str = f"AB: {self.field.batter}"

        rows = [None] * 5
        rows[0] = f"{away_team_str:<6} ┃ {away_team_score:>5} ┃ " \
            f"{second_base_icon:^5}"
        rows[1] = f"{home_team_str:<6} ┃ {home_team_score:>5} ┃ " \
            f"{third_base_icon:<3}{first_base_icon:>2}"
        rows[2] = f"{inning_str:<6} ┃ {count_str:^5} ┃ " \
            f"{outs_str:^5}            "
        rows[3] = pitcher_str
        rows[4] = batter_str
        return "\n".join(rows)


class BoxScore():
    """
    Represents a string for the box score, which includes
    the runs per inning per team (away and home), plus the
    total runs, total hits, and total errors for each team.
    """

    def __init__(self, game: Game):
        self.game = game

    def __str__(self):
        # extra innings pushes prior innings off the left side
        # *_team_box_score_innings is 1-indexed
        first_inning = max(len(self.game.away_team_box_score_innings) - 9, 1)

        box_str_header = ""
        away_box_str = ""
        home_box_str = ""

        for i in range(0, 9):
            inning = first_inning + i
            box_str_header += f"{inning:<3}"

            if inning < len(self.game.away_team_box_score_innings):
                away_box_str += f"{self.game.away_team_box_score_innings[inning]:<3}"
            else:
                away_box_str += "   "

            if inning < len(self.game.home_team_box_score_innings):
                home_box_str += f"{self.game.home_team_box_score_innings[inning]:<3}"
            else:
                home_box_str += "   "

        box_str_header += " R  H  E"
        away_box_str += f" {self.game.away_team_total_runs:<3}"
        away_box_str += f"{self.game.away_team_total_hits:<3}"
        away_box_str += f"{self.game.away_team_total_errors:<2}"
        home_box_str += f" {self.game.home_team_total_runs:<3}"
        home_box_str += f"{self.game.home_team_total_hits:<3}"
        home_box_str += f"{self.game.home_team_total_errors:<2}"

        first_row_str = f"{box_str_header:>{len(box_str_header) + 6}}"
        second_row_str = f"{self.game.away_team:<3} ┃ {away_box_str:>{len(away_box_str)}}"
        third_row_str = f"{self.game.home_team:<3} ┃ {home_box_str:>{len(home_box_str)}}"

        return f"{first_row_str}\n{second_row_str}\n{third_row_str}"


class DisplayText():
    """
    Represents all the text components that are currently displayed by
    the application, namely: the overview pane text, which contains a
    list of all games for the day, and the details pane text, which
    contains the overlay, box score, and play-by-play for the currently
    selected game (or the empty string if no game is selected).
    """

    def __init__(self):
        self.title: str = ""
        self.games: List[Game] = []
        self.open_game: Optional[Game] = None
        self.overlay_str = ""
        self.box_score_str = ""
        self.weather_str = ""
        self.ab_str = ""
        self.play_by_play_str = ""

    def update(self, games: List[Game], date: datetime.date) -> None:
        self.update_overview_pane(games, date)
        self.update_details_pane()
        self.update_weather_pane()

    def update_overview_pane(
            self, games: List[Game], date: datetime.date) -> None:
        if date is None:
            self.title = "Happy offseason!"
        else:
            if date == datetime.date.today():
                self.title = "Today's games"
            else:
                date_str = f"{date.year}-{date.month:02}-{date.day:02}"
                self.title = f"Games on {date_str}"

        for game in games:
            self.games.append(game)

    def update_details_pane(self) -> None:
        if not self.open_game:
            return

        self.overlay_str = str(
            Overlay(self.open_game, self.open_game.field_state))
        self.box_score_str = str(BoxScore(self.open_game))

        play_by_play_str = ""
        for play in reversed(self.open_game.plays):
            if play.description:
                play_by_play_str += play.description + "\n"

        ab_str = ""
        if self.open_game.plays:
            last_play = self.open_game.plays[-1]
            pitch_i = 0
            ab_strs = []
            for event in last_play.sequence:
                if isinstance(event, Pitch):
                    circled_i = chr(ord("①") + pitch_i)
                    ab_strs.append(f"{circled_i:2} {event.short_str()}")
                    pitch_i += 1
                else:
                    ab_strs.append(f"   {event.short_str()}")
            self.ab_str = "\n".join(reversed(ab_strs))

        self.play_by_play_str = play_by_play_str

    def update_weather_pane(self) -> None:
        if not self.open_game or not self.open_game.weather:
            self.weather_str = ""
            return

        weather = self.open_game.weather
        self.weather_str = (f"  {weather.condition_emoji}"
                          + f"  {weather.feelslike_f} °F\n"
                          + f"  {weather.wind_speed_imperial} mph"
                          + f" {weather.wind_direction}\n")

    def select_game(self, game: Game) -> None:
        self.open_game = game
