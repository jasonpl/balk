""" Contains global threads for updating game state. """

import datetime
import logging
import threading
import time

from typing import List, Optional, Tuple

from .statsapi import Stats
from .types import DisplayText, Game, GameState
from .weather import current_weather, has_weather

_UPDATER = None

# The authoritative list of games. These instances are the only ones that
# should exist.
_SCOREBOARD: List[Game] = []
_SCOREBOARD_DATE = datetime.date.today()

# Each game stores its own next_update time for details, but the update
# intervals are controlled here.
_SCOREBOARD_UPDATE_PERIOD = 30
_FOREGROUND_GAME_UPDATE_PERIOD = 10     # match gameday's update frequency
_WEATHER_UPDATE_PERIOD = 60 * 20
_STATS = Stats()


def log_exc(f, *args, **kwargs):
    def wrapped(*args, **kwargs):
        try:
            f(*args, **kwargs)
        except Exception as e:
            logging.exception(f"{f.__name__} threw {e}")
    return wrapped

@log_exc
def _do_updates(display_text: DisplayText) -> None:
    foreground_game = None
    while True:
        for game in _SCOREBOARD:
            if game.next_update_time < time.time():
                logging.info(f"updating {game}")
                # schedule a details update immediately if we've just set a
                # foreground game
                if game == display_text.open_game:
                    if game != foreground_game:
                        foreground_game = game
                        game.next_update_time = 0

                _STATS.update_game_from_details(game)
                interval = _SCOREBOARD_UPDATE_PERIOD
                if game == foreground_game:
                    interval = _FOREGROUND_GAME_UPDATE_PERIOD
                game.next_update_time = time.time() + interval

            if game.game_state == GameState.FINAL:
                # weather for finished games isn't interesting
                game.weather = None
            elif game.next_weather_update_time and game.next_weather_update_time < time.time():
                game.weather = current_weather(
                        game.city if game.use_city_weather else game.stadium)
                if not game.weather and not game.use_city_weather:
                    # couldn't get weather for the stadium, so let's permanently
                    # switch to checking the city and leave the update time so
                    # we try again ASAP
                    logging.info(f"switching to city weather for {game}")
                    game.use_city_weather = True
                else:
                    # got weather, or failed and have nowhere else to fall back
                    # to
                    logging.info(
                            f"updated weather for {game} to {game.weather}")
                    game.next_weather_update_time = \
                            time.time() + _WEATHER_UPDATE_PERIOD

        display_text.update(_SCOREBOARD, _SCOREBOARD_DATE)
        time.sleep(.100)


def start_updater(display_text: DisplayText) -> None:
    global _SCOREBOARD, _SCOREBOARD_DATE, _UPDATER
    latest = _STATS.get_latest_games()
    if latest:
        _SCOREBOARD, _SCOREBOARD_DATE = latest
        _SCOREBOARD.sort()
        for game in _SCOREBOARD:
            if has_weather(game.stadium):
                game.next_weather_update_time = time.time()

        display_text.update(_SCOREBOARD, _SCOREBOARD_DATE)
        _UPDATER = threading.Thread(target=lambda: _do_updates(display_text),
                                    daemon=True)
        _UPDATER.start()
